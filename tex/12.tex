\documentclass[a4paper,11pt,DIV=10]{scrartcl}

\usepackage{fontspec}
\usepackage[ngerman]{babel}
\usepackage[unicode=true]{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{physics}
\usepackage{siunitx}
\usepackage{biblatex}
\usepackage{aligned-overset}
\usepackage{csquotes}
\MakeOuterQuote{"}

\setcounter{section}{11}

\numberwithin{equation}{section}

\theoremstyle{plain}
\newtheorem{thm}{Satz}[section]
\newtheorem{lemm}{Lemma}[section]

\theoremstyle{definition}
\newtheorem{defn}{Def.}[section]

\newcommand*{\diag}{\text{diag}}
\newcommand{\correspondsto}{\overset{\scriptscriptstyle\wedge}{=}\,}

\bibliography{literature.bib}

\begin{document}
\section{SBP-Operatoren für die 2. Ableitung am Beispiel der Wellengleichung}

\subsection{Überblick}
Im Folgenden wird das Lösen der Wellengleichung der Bauart:
\begin{equation}
	a(x)\pdv[2]{u}{t} = \pdv{\left(b(x)\pdv{u}{x}\right)}{x}
\end{equation}
mit $a(x), b(x) > 0$ und der abgekürzten Schreibweise:
\begin{equation}\label{eq:wveq}
	a u_{tt} = \left(b u_x\right)_x
\end{equation}
unter Dirichlet- und gemischten Randbedingungen mithilfe von SBP (Summation by Parts) Operatoren für die zweite Ableitung untersucht.
\\
Bisher:
\begin{itemize}
	\item SBP-Operatoren für Differentialgleichungen mit ersten Ableitungen wie $u_t + a u_x = 0$.
	\item Aus SBP-Eigenschaft Energiestabilität in die Numerik übertragen.
	\item Randbedingungen über SATs (Simultaneous Approximation Terms) eingebaut.
\end{itemize}
Das soll nun auf Glg. \eqref{eq:wveq} übertragen werden.
\\
Vorgehen:
\begin{itemize}
	\item Kontinuierliche Energieabschätzung finden.
	\item Abschätzung diskretisieren und damit SBP-Operatoren konstruieren.
	\item Randwerte mit SATs in SBP-Operator einbauen.
	\item Konstruktion der SATs und Überprüfung von Konvergenz gegen Lösung wird hier nicht betrachtet, da das den Rahmen sprengen würde.
\end{itemize}

\subsection{Definitionen}
\begin{defn}
Das Skalarprodukt für zwei Funktionen $u,v \in L^2([x_L,x_R])$ mit $w(x)>0$ sei:
\begin{equation}
	(u, v)_w = \int_{x_L}^{x_R} uvw \dd{x}.
\end{equation}
\end{defn}
\begin{defn}
Die Norm ist:
\begin{equation}
	\norm{u}_w = \sqrt{(u, u)_w}.
\end{equation}
\end{defn}

\subsubsection{Diskretisierung}
Das Intervall $[x_L,x_R]$, $x_L < x_R$ wird durch $N+1$ äquidistante Positionen:
\begin{equation}
	x_i = i h + x_L,~~~i\in \{0,...,N\},~h = \frac{x_R - x_L}{N}
\end{equation}
diskretisiert. Die approximierte diskrete Lösung wird mit $v^T = [v_0, v_1,..., v_N]$ bezeichnet. Werte am linken bzw. rechten Rand werden mit $v_L$ und $v_R$ abgekürzt.
\begin{defn}
Für $H$ gilt: $H \in \mathbb{R}^{(N+1)\cross(N+1)}$ und $H=H^T>0$ (positiv definit).
\end{defn}
\begin{defn}
Das Skalarprodukt im Diskreten nimmt für $u,v \in \mathbb{R}^{N+1}$ die Form $(u, v)_H = u^THv$ an.
\end{defn}
\begin{defn}
Die Norm ist: $\norm{v}_H = \sqrt{v^THv}$.
\end{defn}
\begin{defn}
Außerdem seien:
\begin{equation}
	e_L = [1, 0, ..., 0]^T, ~~~~ e_R = [0, ..., 0, 1]^T.
\end{equation}
\end{defn}

\subsection{Wie sieht die Energieabschätzung aus?}
\begin{defn}
	Die Energie sei:
	\begin{equation}
		E = \norm{u_t}_a^2 + \norm{u_x}_b^2.
	\end{equation}
\end{defn}
Betrachte:
\begin{align*}
	\dv{t} E &= \dv{t} \left[ \int_{x_L}^{x_R} a u_t^2 \dd{x} + \int_{x_L}^{x_R} b u_x^2 \dd{x} \right] \\
			 &= \int_{x_L}^{x_R} 2 u_t a u_{tt} \dd{x} + \int_{x_L}^{x_R} 2 b u_x u_{xt} \dd{x} \\
			 \overset{\eqref{eq:wveq}}&{=} \int_{x_L}^{x_R} 2 u_t (b u_x)_x \dd{x} + \int_{x_L}^{x_R} 2 b u_x u_{xt} \dd{x} \\
			 \overset{\text{P.I.}}&{=} 2 b u_t u_x |_{x_L}^{x_R} - \int_{x_L}^{x_R} 2 u_{tx} b u_x \dd{x} + \int_{x_L}^{x_R} 2 b u_x u_{xt} \dd{x} \\
			 &= 2 b u_t u_x |_{x_L}^{x_R}.
\end{align*}
Diese Definition ist sinnvoll, da sie der physikalischen Energie entspricht. Für homogene Dirichlet ($u(t, x_L)=u(t, x_R)=0$) oder Neumann ($u_x(t, x_L)=u_x(t, x_R)=0$) Randbedingungen liefert dies wie erwartet eine konstante Energie.

\subsection{Konstruktion eines passenden SBP-Operators}
Idee: Liebevoller Blick auf die partielle Integration im Kontinuierlichen und Diskretisierung einzelner Operationen:
\begin{equation}
	\underbrace{\int_{x_L}^{x_R} u (b v_x)_x \dd{x}}_{\correspondsto u^T H (b v_x)_x} = \underbrace{u b v_x |_{x_L}^{x_R}}_{\correspondsto u^T B S v} - \underbrace{\int_{x_L}^{x_R} u_x b v_x \dd{x}}_{\correspondsto u^T M_b v}.
\end{equation}
Übertrage Eigenschaften der kontinuierlichen Operationen in die Diskretisierung:
\begin{itemize}
	\item $H$ bildet ein Skalarprodukt nach $\rightarrow$ $H=H^T>0$.
	\item $B$ soll die Auswertung der Ränder mit $b$ multipliziert liefern \\ $\rightarrow$ $B = diag(-b_L, 0, ..., 0, b_R)$ mit $b_L = b(x_L)$, $b_R = b(x_L)$.
	\item $S$ steht für die Ableitung am Rand und muss diese entsprechend approximieren
	\item $M_b$ steht für das Skalarprodukt über die Ableitungen gewichtet mit $b$ \\ $\rightarrow$ $M_b=M_b^T \geq 0$ (nur semidefinit wegen der Ableitung).
\end{itemize}
Erhalte nun durch Umformen der diskreten Version einen Operator für die zweite Ableitung:
\begin{align*}
	u^T H (b v_x)_x &= u^T B S v - u^T M_b v \\
	\Leftrightarrow H (b v_x)_x &= B S v - M_b v \\
	\Leftrightarrow (b v_x)_x &= H^{-1}(- M_b v + B S v) \\
							  &= H^{-1}(- M_b + B S) v.
\end{align*}
Dies motiviert:
\begin{defn}
	Sei $D_2^{(b)} = H^{-1}(- M_b + B S)$ eine Approximation an $\pdv*{x}(b \pdv*{x})$ mit $b(x) > 0$, ausreichend glatt, mit innerem Stencil $p$-ter Ordnung. $D_2^{(b)}$ heißt SBP-Operator $p$-ter Ordnung für zweite Ableitungen, wenn $H$ diagonal und positiv definit ist, $M_b$ symmetrisch und positiv semidefinit ist, S die erste Ableitung an den Rändern approximiert und $B = diag(-b_L, 0, ..., b_R)$. \footnote{Im folgenden wird von SBP-Operatoren mit sogenannten "accurate narrow Stencils" ausgegangen. Für eine genaue Definition siehe \cite{StableBoundarySBPSecondOrder}.}
\end{defn}

\subsubsection{Überprüfung der SBP-Eigenschaft}
\begin{defn}
\begin{equation}
	A = \diag(a(x_0), a(x_1), ..., a(x_N)).
\end{equation}
\end{defn}
\begin{defn}
Die Semidiskretisierung von Glg. \eqref{eq:wveq} lautet:
\begin{equation}\label{eq:sd_wveq}
	A v_{tt} = D_2^{(b)} v.
\end{equation}
\end{defn}
\begin{defn}
	Die semidiskrete Energie sei:
	\begin{equation}
		E_H = \underbrace{\norm{v_t}_{HA}^2}_{\correspondsto \norm{v_t}_a^2} + \underbrace{v^T M_b v}_{\correspondsto \norm{v_x}^2}.
	\end{equation}
\end{defn}
Es folgt für die Zeitableitung:
\begin{align*}
	\dv{t} E_H &= v_{tt}^T HA v_t + v_t^T HA v_{tt} + v_t^T M_b v + v^T M_b v_t \\
			   &= 2 v_t HA v_{tt} + 2 v^T_t M_b v \\
			   \overset{\eqref{eq:sd_wveq}}&{=} 2 v_t H H^{-1}(- M_b + B S) v + 2 v_t^T M_b v \\
			   &= 2 v_t (- M_b + B S) v + 2 v_t^T M_b v \\
			   &= 2 v_t B S v = 2v_{tL}(BSv)_L + 2v_{tR}(BSv)_R.
\end{align*}
Dies entspricht der Struktur des kontinuierlichen Falls.

\subsection{Randbedingungen und SATs}
Jetzt müssen Randbedingungen in den SBP-Operator eingebaut werden.
Neben den Neumann-Randbedingungen:
\begin{subequations}\label{eqs:dirichlet}
\begin{align}
	u_x(t, x_L) &= g_{NL}(t) \\
	u_x(t, x_R) &= g_{NR}(t)
\end{align}
\end{subequations}
sollen hier vornehmlich gemischte und Dirichlet-Randbedingungen betrachtet werden:
\begin{subequations}
\begin{align}
	u(t, x_L) &= g_{DL}(t) \\
	u(t, x_R) &= g_{DR}(t).
\end{align}
\end{subequations}
\begin{itemize}
	\item Injektionsmethode (Gleichsetzen der numerischen Lösung mit Randwerten) \\ $\rightarrow$ zerstört SBP-Eigenschaft.
	\item Idee: "Strafterme" lenken numerische Lösung je nach Abweichung von Randwerten zurück \\ $\rightarrow$ Simultaneous Approximation Terms (SATs).
\end{itemize}

\subsection{Gemischte Randbedingungen}
Allgemeine Randbedingungen:
\begin{subequations}
\begin{align}
	L_Lu &= \beta_1u - \beta_2bu_x + \beta_3u_t = g_L(t), ~~~ x=x_L, \\
	L_Ru &= \beta_1u + \beta_2bu_x + \beta_3u_t = g_R(t), ~~~ x=x_R.
\end{align}
\end{subequations}
Für $\beta_2 > 0$, $g_L\equiv g_R \equiv 0$ \footnote{Zitat aus \cite{StableBoundarySBPSecondOrder}: "The analysis holds for inhomogenous data, but introduces unnecessary notation."} betrachte:
\begin{align*}
	&\dv{t} \left( \norm{u_t}_a^2 + \norm{u_x}_b^2 + \frac{\beta_1}{\beta_2}u_L^2 + \frac{\beta_1}{\beta_2}u_R^2 \right) \\
	&= 2u_tbu_x|_{x_L}^{x_R} + 2\frac{\beta_1}{\beta_2}(u_Lu_{tL} + u_Ru_{tR}) \\
	&= \frac{2}{\beta_2}\left[ u_{tL}(\beta_1 u_L - \beta_2 bu_{xL}) + u_{tR}(\beta_1 u_R + \beta_2 bu_{xR}) \right] \\
	&= \frac{2}{\beta_2}\left[ u_{tL}(-\beta_3 u_{tL}) + u_{tR}(-\beta_3 u_{tR}) \right] \\
	&= -\frac{2\beta_3}{\beta_2} u_{tL}^2 - \frac{2\beta_3}{\beta_2} u_{tR}^2.
\end{align*}
Für
\begin{equation}\label{eqs:beta123}
	\frac{\beta_1}{\beta_2} \geq 0, ~~~ \frac{\beta_3}{\beta_2} \geq 0
\end{equation}
folgt Energiestabilität, denn:
\begin{subequations}
\begin{align}
	&E = \norm{u_t}_a^2 + \norm{u_x}_b^2 \leq \norm{u_t}_a^2 + \norm{u_x}_b^2 + \frac{\beta_1}{\beta_2}u_L^2 + \frac{\beta_1}{\beta_2}u_R^2 \\
	&\dv{t} \left(\norm{u_t}_a^2 + \norm{u_x}_b^2 + \frac{\beta_1}{\beta_2}u_L^2 + \frac{\beta_1}{\beta_2}u_R^2\right) \leq 0.
\end{align}
\end{subequations}
Warum diese Rechnung? - In der Diskretisierung ergibt sich durch die im Folgenden gewählten SATs eine analoge Abschätzung.

\begin{thm}
\begin{defn}
Sei eine Semidiskretisierung mit SATs der Glg. \eqref{eq:wveq} gegeben durch:
\begin{equation}
	HAv_{tt} = (-M_b +BS)v + \tau e_L(L^T_Lv-g_L) + \tau e_R(L^T_Rv-g_R)
\end{equation}
mit
\begin{subequations}
\begin{align}
	L^T_Lv &= \beta_1 v_L + \beta_2(BSv)_L + \beta_3 v_{tL} \\
	L^T_Rv &= \beta_1 v_R + \beta_2(BSv)_R + \beta_3 v_{tR}.
\end{align}
\end{subequations}
\end{defn}
Diese Diskretisierung ist energiestabil wenn es sich um einen SBP-Operator für zweite Ableitungen mit homogenen Randwerten handelt, Glgn. \eqref{eqs:beta123} gelten und $\tau = -\frac{1}{\beta_2}$.
\begin{proof}
Betrachte:
\begin{align*}
	\tilde{E}_H &= \norm{v_t}_{HA}^2 + v^TM_bv - \tau\beta_1 v_L^2 - \tau\beta_1 v_R^2 \\
	\dv{t} \tilde{E}_H &= 2 v_tHAv_{tt} + 2 v_t^TM_bv - 2\tau\beta_1 v_Lv_{tL} - 2\tau\beta_1 v_Rv_{tR} \\
	&= 2 v_t\left[(-M_b +BS)v + \tau e_LL^T_Lv + \tau e_RL^T_Rv\right] + 2 v_t^TM_bv - 2\tau\beta_1 v_Lv_{tL} - 2\tau\beta_1 v_Rv_{tR} \\
	&= 2 [v_{tL}(BSv)_L + v_{tR}(BSv)_R + v_{tL}\tau(\beta_1 v_L + \beta_2(BSv)_L + \beta_3 v_{tL})\\
	&~~~ + v_{tR}\tau (\beta_1 v_R + \beta_2(BSv)_R + \beta_3 v_{tR})] -2\tau\beta_1 v_Lv_{tL} - 2\tau\beta_1 v_Rv_{tR} \\
	&= 2\tau\beta_3v_{tL}^2 + 2\tau\beta_3v_{tL}^2 + 2(1+\tau\beta_2)v_{tL}(BSv)_L + 2(1+\tau\beta_2)(v_{tR}(BSv)_R.
\end{align*}
Für $\tau = -\frac{1}{\beta_2}$ folgt:
\begin{subequations}
\begin{align}
	E_H &\leq \tilde{E}_H \\
	\dv{t} \tilde{E}_H &= -2\frac{\beta_3}{\beta_2}v_{tL}^2 - 2\frac{\beta_3}{\beta_2}v_{tL}^2 \leq 0.
\end{align}
\end{subequations}
\end{proof}
\end{thm}

\subsection{Homogene Dirichlet-Randbedingungen}
Mit homogenen Dirichlet-Randbedingungen ist das Nullsetzen der Randwerte in \eqref{eqs:dirichlet} gemeint.
Damit folgt:
$$\dv{t} E = 2 b u_t u_x |_{x_L}^{x_R} = 0.$$
Finden nun SATs, die die Randbedingungen einbauen und die Diskrete Energie konstant halten.
Brauche erst:
\begin{lemm}
	$M_b$ aus einem SBP-Operator für zweite Ableitungen lässt sich darstellen durch:
	\begin{equation}
		v^TM_bv = h\frac{\alpha}{b_L}(BSv)_L^2 + h\frac{\alpha}{b_R}(BSv)_R^2 + v^T\tilde{M}_bv
	\end{equation}
	mit: $\tilde{M}_b = \tilde{M}_b^T$, positiv semidefinit und $\alpha > 0$ und unabhängig von $h$.
	\\
	Wie wird $\alpha$ bestimmt? $\rightarrow$ Maple. Für Details siehe \cite{StableWaveDiscMediaSBP}.
\end{lemm}

\begin{thm}
\begin{defn}
Seien SATs gegeben durch:
\begin{equation}
	\begin{split}
	HAv_{tt} = (-M_b + BS)v + \epsilon (BS)^Te_L(v_L-g_{DL}) + \sigma b_L e_L(v_L-g_{DL}) + \\ \epsilon (BS)^Te_R(v_R-g_{DR}) + \sigma b_R e_R(v_R-g_{DR}).
	\end{split}
\end{equation}
\end{defn}
Dann ist die Diskretisierung für $g_{DL} \equiv g_{DR} \equiv 0$ energiestabil\footnote{Um genau zu sein wird hier die Definition der diskreten Energie etwas abgewandelt, bleibt aber eine (Halb-)Norm.}, falls es sich bei der Diskretisierung um einen SBP-Operator für zweite Ableitungen handelt, $\sigma \leq -\frac{1}{\alpha h}$ und $\epsilon = 1$.
\end{thm}
\begin{proof}
Seien:
$$w_L^T = [v_L, (BSv)_L], ~~~~ w_R^T = [v_R, (BSv)_R],$$
$$R_L =
\begin{bmatrix}
	-\sigma b_L & -1 \\
	-1 & \frac{\alpha h}{b_L}
\end{bmatrix},
~~~~ R_R =
\begin{bmatrix}
	-\sigma b_R & -1 \\
	-1 & \frac{\alpha h}{b_R}
\end{bmatrix}
.$$
Kann zeigen $R_L$ und $R_R$ haben für $\sigma \leq -\frac{1}{\alpha h}$ nicht negative Eigenwerte.
Dann ist:
\begin{align*}
	&\dv{t} \left( \norm{v_t}_{HA}^2 + w_L^TR_Lw_L + w_R^TR_Rw_R + v^T\tilde{M}_bv \right) \\
	&=\dv{t} \left( v_t^THAv_t + \underbrace{h\frac{\alpha}{b_L}(BSv)_L^2 + h\frac{\alpha}{b_R}(BSv)_R^2 + v^T\tilde{M}_bv}_{=v^TM_bv} \right. \\
	&~~~ \left. \vphantom{\underbrace{\frac{\alpha}{b_L}}_{=v^TM_bv}} - 2(BSv)_Lv_L - \sigma b_Lv_L^2 - 2(BSv)_Rv_R - \sigma b_Rv_R^2 \right) \\
	&=  2v_tHAv_{tt} + 2v_t^TM_bv - 2(BSv_t)_Lv_L - 2 \sigma b_Lv_Lv_{tL} - 2(BSv_t)_Rv_R - 2 \sigma b_Lv_Lv_{tR} \\
	&= 2v_{tL}(BSv)_L + 2v_{tR}(BSv)_R \\
	&~~~ + 2v_t^T\left[(BS)^Te_Lv_L + \sigma b_L e_Lv_L + (BS)^Te_Rv_R + \sigma b_R e_Rv_R\right] \\
	&~~~ - 2(BSv_t)_Lv_L - 2 \sigma b_Lv_Lv_{tL} - 2(BSv_t)_Rv_R - 2 \sigma b_Lv_Lv_{tR} \\
	&~~~ - 2(BSv)_Lv_{tL} - 2(BSv)_Rv_{tR} = 0.
\end{align*}
\end{proof}

\printbibliography

\end{document}
